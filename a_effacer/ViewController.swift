//
//  ViewController.swift
//  a_effacer
//
//  Created by Alain on 16-08-17.
//  Copyright © 2016 Production sur support. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITextFieldDelegate {

    let taxes:Float = 1.15
    let TPS:Float = 0.05
    let TVQ:Float = 0.09975
    
    @IBOutlet weak var quantité: UITextField!
    @IBOutlet weak var prix: UITextField!
    @IBOutlet weak var total: UITextField!
    @IBOutlet weak var totalAvecTaxes: UITextField!
    
    @IBOutlet weak var tps: UILabel!
    
    @IBOutlet weak var tvq: UILabel!
    
    @IBAction func calculerTotalAvecTaxes(_ sender: AnyObject) {
        if total.text?.isEmpty == false {
            
            let _tps = Float(total.text!)! * TPS
            let _tvq = Float(total.text!)! * TVQ
            let _totalAvecTaxes = Float(total.text!)! + _tps + _tvq
            
            tps.text = NSString(format: "%2.2f$", _tps) as String
            tvq.text = NSString(format: "%2.2f$", _tvq) as String
            totalAvecTaxes.text = NSString(format: "%2.2f$", _totalAvecTaxes) as String
            
            
        } // if
    } // func calculerTotalAvecTaxes
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        quantité.text = nil
        prix.text = nil
        total.text = nil
        tps.text = nil
        tvq.text = nil
        totalAvecTaxes.text = nil
    }

    
    func afficherAlert(_ message:String) {
        let alert = UIAlertController(title: "Attention!", message: message, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK, je ferai plus attention!", style: UIAlertActionStyle.default, handler: nil))
        // Patch temporaire pour Xcode 8b5; utilisation de 'DispatchQueue' pour exécuter le self.present()
        DispatchQueue.main.async( execute: { self.present(alert,  animated: true, completion: nil) } )
        //self.present(alert,  animated: true, completion: nil)
    
    }
    
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        print("#L'utilisateur est entré dans la zone de texte!")
        textField.textColor = UIColor.red
    }

    func textFieldDidEndEditing(_ textField: UITextField) {
        textField.textColor = UIColor.black
        if textField.text!.isEmpty == true {
          afficherAlert("Le champ de saisie ne doit pas être vide!")
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
       
        if let _ = Float(textField.text!) {
            textField.resignFirstResponder()
            if let _quantité = Float(quantité.text!), let _prix = Float(prix.text!) {
                // total.text = String(_quantité * _prix)
                total.text = NSString(format: "%2.2f", _quantité * _prix) as String
                calculerTotalAvecTaxes(self)
            } // if
        } else
        {
             afficherAlert("Erreur, Il faut entrer un nombre!")
            return false
        }  // if
        
        return true
    }  // textFieldShouldReturn
}

